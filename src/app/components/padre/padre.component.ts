import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  padretexto="compra tu almuerzo"
  textopadre2="falta cambio"

  listacompras = {
    frutas:'durazno, sandia,piña, tomate',
    verduras:'zanahoria, pepino',
    abarrotes:'aceite, arroz'
  }

  compramaterialescolar = {
    cuadernos:'50 hojas, anillados, tm carta',
    boligrafo:'negro,azul,morado,rojo',
    mochila:'Hummer'
  }

  constructor() {
  
   }

  ngOnInit(): void {
  }

}
